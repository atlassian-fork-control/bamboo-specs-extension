package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.plan.Plan
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties.NotificationStrategy.*
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties.TriggeringOption.INHERITED
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties.TriggeringOption.MANUAL

sealed class BranchManagementType : PlanBranchManagement() {
    class Manual : BranchManagementType() {
        init {
            createManually()
        }
    }

    class Vcs : BranchManagementType() {
        init {
            createForVcsBranch()
        }

        fun pattern(pattern: String) {
            createForVcsBranchMatching(pattern)
        }
    }

    class PullRequest : BranchManagementType() {
        init {
            createForPullRequest()
        }
    }
}

fun <T : BranchManagementType> Plan.planBranchManagement(branchManagement: () -> T,
                                                         notification: PlanBranchManagementProperties.NotificationStrategy? = null,
                                                         trigger: PlanBranchManagementProperties.TriggeringOption? = null,
                                                         init: SpecsDsl<T> = {}) {
    val management = branchManagement()
    management.init()
    when (trigger) {
        INHERITED -> management.triggerBuildsLikeParentPlan()
        MANUAL -> management.triggerBuildsManually()
        null -> Unit
        else -> UnsupportedOperationException("$trigger not supported")
    }
    when (notification) {
        NOTIFY_COMMITTERS -> management.notificationForCommitters()
        INHERIT -> management.notificationLikeParentPlan()
        NONE -> management.notificationDisabled()
        null -> Unit
        else -> UnsupportedOperationException("$notification not supported")
    }
    this.planBranchManagement(management)
}

fun PlanBranchManagement.cleanup(afterRemoval: Boolean, afterInactivity: Boolean,
                                 daysAfterRemoval: Int? = null,
                                 daysAfterInactivity: Int? = null,
                                 init: SpecsDsl<BranchCleanup> = {}) {
    val branchCleanup = BranchCleanup()
    if (afterRemoval && daysAfterRemoval != null)
        branchCleanup.whenRemovedFromRepositoryAfterDays(daysAfterRemoval)
    else
        branchCleanup.whenRemovedFromRepository(afterRemoval)

    if (afterInactivity && daysAfterInactivity != null)
        branchCleanup.whenInactiveInRepositoryAfterDays(daysAfterInactivity)
    else
        branchCleanup.whenInactiveInRepository(afterRemoval)

    branchCleanup.init()
    this.delete(branchCleanup)
}
